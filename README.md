# Overview

For details on executing the emulations, clone this repository to an EMANE node
equipped with [ETCE](https://github.com/adjacentlink/python-etce) and run:

```
cd emulations
etce-test list -v TEST_DIR
```

Where `TEST_DIR` is the directory name of any of the emulations in this
repository.

---

Copyright (c) - 2024 The MITRE Corporation. All Rights Reserved.

Public release case number:  24-1190
